
# LIS4369 Extensible Enterprise Solutions

## Chris Ehster

### Project 2 Requirements:
1. Distributed Version Control with Git and Bitbucket
2. Backwards-Engineer an application in R using provided screenshots
3. Write an application that collects information about Motor Trends Cars
   1. Display summary statistics and `dataframe` information about dataset 
   2. Use `qplot()` and `plot()` to visualize data 


#### README.md file should include the following items:

* Screenshot of source code snippet
* Screenshot of application running
* git commands w/short descriptions

#### Git commands w/short descriptions:

1. git init - This command creates a git repository.
2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).

#### Assignment Screenshots:

*screenshot of p2 code*:

![Code](img/vsc.png)

*Screenshot of p2 running*:

![VSC Screenshot](img/code1.png)

*Screenshot of p2 running*:

![VSC Screenshot](img/code2.png)

*Screenshot of p2 qplot*:

![Screenshot](img/qplot.png)

*Screenshot of p2 plot*:

![Screenshot](img/plot.png)




