
# LIS4369 Extensible Enterprise Solutions

## Chris Ehster

### Assignment 4 Requirements:
1. Distributed Version Control with Git and Bitbucket
2. Install several data analytics packages with PIP (including pandas, matplotlib, numpy, etc.)
3. Write an application that collects information about Titanic passengers
   1. Use `DataFrame` objects from the pandas module to display summary statistics and select data segments
   2. Use the matplotlib module to produce a visualization of the data

The matplotlib graph has a gap in the line because there is no entry for the 'Age' field for some passengers.


#### README.md file should include the following items:

* Screenshot of source code snippet
* Screenshot of application running
* git commands w/short descriptions

#### Git commands w/short descriptions:

1. git init - This command creates a git repository.
2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).

#### Assignment Screenshots:

*screenshot of a4 code*:

![Code](img/vscCode.png)

*Screenshot of a4 running (Visual Studio Code)*:

![VSC Screenshot](img/vscOutput.png)

*Screenshot of a4 running (IDLE)*:

![IDLE Screenshot](img/IDLEOutput.png)




