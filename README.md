# LIS4369 - Extensible Enterprise Solutions

## Chris Ehster

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create a1_tip_calculator application
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Import Function module
    - Create a2 application
    - Provide screenshots of application running
    - Create Bitbucket repo

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Use looping control structure for program flow
    - Create the A3 application
    - Provide screenshots of application running
    - Create Bitbucket repo

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Install data analytics modules with PIP
    - Collect information about Exxon Mobil over several years
      - Store information in a pandas `DataFrame` object
    - Visualize collected information with matplotlib

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Install and configure Pandas, NumPy, and Matplotlib
    - Collect information about Titanic passengers
      - Store collected information in `DataFrame` object
      - Use data slicing to select segments of data
      - Create an N-dimensional array from `DataFrame` object
    - Visualize passenger ages with matplotlib

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Demonstrate basic features of the R programming language in RStudio
      - Assignment operator  `<-` 
      - Scalar vs Non-Scalar data types 
    - Generate basic summary statistics using built-in R functions
      - Manage incomplete datasets using the 'Titanic' `.csv` file
    - Generate data visualizations using built-in R functions

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Backwards-Engineer an R application from given screenshots
    - Collect information about Motor Trends cars
      - Display summary statistics and information about `dataframes`
    - Visualize collected information with `qplot()` and `plot()`