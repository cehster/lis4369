
# LIS4369 Extensible Enterprise Solutions

## Chris Ehster

### Assignment 2 Requirements:
1. Distributed Version Control with Git and Bitbucket
2. Import logic for the questions from a separate module to be called by `main.py`
3. Write an application that calculates hourly wages, taking overtime pay and holiday rate increases into account.
    a. Conditional Logic
    b. Functions
    c. Modules
    d. String Formatting


#### README.md file should include the following items:

* Screenshot of source code snippet
* Screenshot of application running
* git commands w/short descriptions

#### Git commands w/short descriptions:

1. git init - This command creates a git repository.
2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).

#### Assignment Screenshots:

*screenshot of a2 code*:

![Code](img/code.png)

*Screenshot of a2 running (Visual Studio Code)*:

![VSC Screenshot](img/vscOutput.png)

*Screenshot of a2 running (IDLE)*:

![IDLE Screenshot](img/idleOutput.png)




